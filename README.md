LEAVEAMESSAGE is a handmade network of voicemailboxes that allow members of the public to record and distribute audio messages in public space. 
Each box is a simple device that has the functions to record and playback audio. 
It has two buttons. One plays back the last recorded message and the other one allows people to make a new recording. 
There are a few LEAVEAMESSAGE boxes in different cities, countries, contexts. All boxes connected to each other. 
As soon as a new recording is made it becomes the one played on all of the boxes, until a new one appears. 


# Links

http://oyoana.com/leaveamessage/
