#!/usr/bin/env python
import os, subprocess, signal, time
import RPi.GPIO as GPIO
import datetime

PIR = 23
LED = 24
BUTTON = 25

pirState = False
pirVal = False
#pirValButton = False

initialRecTime = 0.0
recordvalue = False

#maximum duration of the sound file
maxRecordingTime = 60.0

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR,GPIO.IN)
GPIO.setup(LED,GPIO.OUT)
GPIO.setup(BUTTON,GPIO.IN)

#static starts and ends of the commands, the counter variable goes in between them 
myname = 'NAME-GOES-HERE-'
commandRecordBegin='killall arecord; arecord -q -f cd -t wav > file sounds/'
commandRecordEnd='.wav'
commandPlayBegin='aplay '
lastSoundName = ' ' 

from subprocess import check_output, CalledProcessError


FOLDER = "/home/pi"


def get_free():
	i = os.statvfs(FOLDER)
	free_mb = (i.f_bavail * i.f_frsize) / 1024.0 / 1024.0
	return free_mb

def delete_if_full():
	print "delete_if_full"
	free = get_free()
	minfree = 275.0
	while (free < minfree):
		print free
		sfiles = check_output(["/bin/sh", "-c", "ls -t /home/pi/sounds/*wav"]).strip().split("\n")
		oldest = sfiles[-1]
		oldest = os.path.join ("/home/pi/sounds/", oldest)
		print oldest
		print 'I am in the loop'
		os.remove (oldest)  #deleted oldest file
		print "deleted oldest"
		free = get_free()
		print free
	 
	

def play():
	print "PLAY"
	try:
		
		
		cmd = "killall arecord; killall aplay; ssh -C user@your-server-ip './retrieveLatest' | aplay"
		print "CMD:", cmd
		print "SSH THREE"
                os.system(cmd)

	except CalledProcessError:
		print "oops, error ls ing server. Playing local files."
		playLocal()
	except IndexError:
		print "oops, error ls ing server. Playing local files."
		playLocal()

def playLocal():
	print "PLAY LOCAL"
	lsLocalSounds = check_output(["ls -t sounds/*.wav"], shell=True)
	mostRecentSound = lsLocalSounds.split("\n")[0]
	print mostRecentSound
	commandPlay=commandPlayBegin+mostRecentSound
	os.system(commandPlay)	

def stopRecording():
	global recordvalue
	global initialRecTime
	recordvalue = False 
        		
	print "stop recording called"
	GPIO.output(LED, False)
	##os.kill(proc1.pid, signal.SIGKILL)
	##os.kill(proc1.pid+1, signal.SIGKILL)
        print "CMD: killall arecord"
	os.system('killall arecord')

	currentRecTime = time.time() 
	diffRecTime = currentRecTime - initialRecTime
        if diffRecTime < 1.0:
        	print "Too small, didn't save"
	else:
		print "finished recording and killed process"
		scpCommandStr = "scp -C sounds/"+lastSoundName+" user@your-server-ip:leaveamessage/sounds/"
		os.system(scpCommandStr)
		delete_if_full()	

	initialRecTime = 0.0	


#We use this variable for forcing the user to release the buttons to start recording or playing again
needToReleaseRec = False
needToReleasePlay = False
while True:
	pirValButton = GPIO.input(BUTTON)
	if(pirValButton == True):
		if(recordvalue == False and needToReleaseRec == False):
			recordvalue = True
			needToReleaseRec = True
			initialRecTime = time.time()
			print "initialTime",initialRecTime
			GPIO.output(LED, True)
			print "REC"
			timestampNow = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
			lastSoundName = myname+timestampNow+commandRecordEnd
			commandRecord=commandRecordBegin+lastSoundName
			proc1 = subprocess.Popen(commandRecord, shell = True)
			print "hello stranger"
			##os.system(commandRecord)
	
	
	else:
		needToReleaseRec = False
	if(recordvalue == True):
		if(pirValButton == False):
			stopRecording()
		else:
			currentRecTime = time.time() 
			diffRecTime = currentRecTime - initialRecTime
			if(diffRecTime > maxRecordingTime):
				print "You pressed the REC button for too long! ",diffRecTime
				stopRecording()
	
		
	if(pirValButton == False): #in case the REC button is no pressed we check the PLAY one
		pirVal = GPIO.input(PIR)		
		if(pirVal == True):
                        print "PLAY PRESSED"
			if(needToReleasePlay == False):
				needToReleasePlay = True
				print 'PLAY button pressed'
				play()
				print 'back from playing'
		else:
			needToReleasePlay = False

